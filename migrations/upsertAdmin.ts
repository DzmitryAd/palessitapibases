//ts-node -r dotenv/config -P migrations/tsconfig.json  migrations/upsertAdmin.ts
import {
    upsertAdmin,
    getAdminByEmail,
    IAdminJSON
  } from "../server/models/AdminModel"
import { prompt } from "../util/prompt"

  const upsertAdminMigration = async () => {
    const email = await prompt("Enter email: ")
    const dbAdminJSON = await getAdminByEmail(email)

    if (dbAdminJSON) {
      console.log("Editing admin: ", JSON.stringify(dbAdminJSON, null, 2))
    } else {
      console.log("Creating new admin: ")
    }

    const name = await prompt("Enter name: ")
    const password = await prompt("Enter password: ")

    const admin: IAdminJSON = { email } as IAdminJSON
    if (password) {
      admin.password = password
    }

    if (name) {
      admin.name = name
    }

    upsertAdmin(admin, dbAdminJSON && dbAdminJSON.admin_id)
      .then(() => {
        console.log("Success: ", JSON.stringify(admin, null, 2))
        process.exit()
      })
      .catch(e => {
        console.error("Error: ", e)
        process.exit()
      })
  }

  upsertAdminMigration()
