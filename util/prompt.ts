export const prompt = (question: string): Promise<string> => {
  var rl = require("readline")
  var r = rl.createInterface({
    input: process.stdin,
    output: process.stdout,
    terminal: false
  })
  return new Promise((resolve, error) => {
    r.question(question, answer => {
      r.close()
      resolve(answer)
    })
  })
}

