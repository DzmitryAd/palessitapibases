interface IProcessEnv {
    NODE_ENV: string
    PORT: string | number
    DB_HOST: string
    DB_USER: string
    DB_PASS: string
    DB_NAME: string
    DB_URL?: string
}

export interface IEnv extends IProcessEnv {
    IS_NODE_PROD: boolean
}

export const env: IEnv = {
    NODE_ENV: process.env.NODE_ENV || "production",
    PORT: process.env.PORT || 3000,
    DB_HOST: process.env.DB_HOST,
    DB_USER: process.env.DB_USER,
    DB_PASS: process.env.DB_PASS,
    DB_NAME: process.env.DB_NAME,
    DB_URL: process.env.DB_URL,
  
    // additional
    IS_NODE_PROD: process.env.NODE_ENV === "production"
}