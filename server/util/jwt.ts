import * as jwt from "jwt-simple"

export const secret = Buffer.from(
  "0d16b31f0ccd6b20e5de616d579cfc0a",
  "hex"
).toString()

export interface JwtCreatorOptions {
  admin_id: string | number
}

export interface JwtEncoderOptions {
  admin_id: string | number
}

export function createJwt(jwtOptions: JwtCreatorOptions) {
  const jwtFields: JwtEncoderOptions = {
    admin_id: jwtOptions.admin_id
  }
  return jwt.encode(jwtFields, secret)
}
