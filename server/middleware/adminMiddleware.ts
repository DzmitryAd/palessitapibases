import * as jwt from "jwt-simple"
import { Context, Middleware } from "koa"

import { IAppContext, ContextAdmin } from "../models/App"
import { secret, JwtEncoderOptions } from "../util/jwt"
import { getAdminById } from "../models/AdminModel"

export const adminMiddleware: Middleware = async (ctx: IAppContext, next) => {
  const token = ctx.get("Authorization")

  if (token) {
    const jwtPayload = jwt.decode(token, secret) as JwtEncoderOptions
    const admin = await getAdminById(jwtPayload.admin_id)

    const contextAdmin = new ContextAdmin({
      admin_id: admin.admin_id,
      email: admin.email,
      name: admin.name
    })
    ctx.state.admin = contextAdmin
  }

  await next()
}
