export const errorsMiddleware = async (ctx, next) => {
  try {
    await next()
  } catch (err) {
    console.log("err", err)
    ctx.status = err.status || 500
    ctx.type = "json"
    const msg =
      typeof err.message === "object" ? err.message : { error: err.message }
    ctx.body = msg
    ctx.app.emit("error", err, this)
  }
}
