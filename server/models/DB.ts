import * as knex from "knex"
import { env } from "../util/env"

export const db = knex({
  client: "pg",
  connection: env.DB_URL || {
    host: env.DB_HOST,
    user: env.DB_USER,
    password: env.DB_PASS,
    database: env.DB_NAME
  },
  debug: false
})

export const promisify = <T>(fn) =>
  new Promise<T>((resolve, reject) => fn(resolve))
