import { Context, Middleware } from "koa"

// ADMIN
interface IContextAdmin {
  admin_id: string | number
  email: string
  name: string
}

export class ContextAdmin implements IContextAdmin {
  admin_id: string | number
  email: string
  name: string
  constructor(props: IContextAdmin) {
    Object.assign(this, props)
  }
}

export interface IAppContext extends Context {
  state: {
    admin: ContextAdmin
  }
}
