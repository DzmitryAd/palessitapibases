import * as bcrypt from "bcrypt"
import { Transaction, QueryBuilder } from "knex"
import { db, promisify } from "./DB"

const tableNameAdmin = "admin"

export interface IAdminJSON {
    admin_id?: number | string
    name: string
    password?: string
    email: string
}

export const upsertAdmin = async (
  adminJSON: Partial<IAdminJSON>,
  admin_id?: string | number
): Promise<IAdminJSON> => {
  const adminToSave = {} as IAdminJSON
  const dbAdminJSON: IAdminJSON = admin_id
    ? await getAdminById(admin_id)
    : {} as IAdminJSON

  if (adminJSON.password !== undefined) {
    const salt = bcrypt.genSaltSync()
    adminToSave["password"] = bcrypt.hashSync(adminJSON.password, salt)
  }
  if (adminJSON.email !== undefined) {
    adminToSave["email"] = adminJSON.email
  }
  if (adminJSON.name !== undefined) {
    adminToSave["name"] = adminJSON.name
  }

  const trx = await promisify<Transaction>(db.transaction)
  let upsertAdminId = admin_id ? admin_id : null
  try {
    if (Object.keys(adminToSave).length > 0) {
      if (admin_id) {
        await trx(tableNameAdmin)
          .where({ admin_id: admin_id })
          .first()
          .update(adminToSave)
      } else {
        console.log(adminToSave)
        const [newAdmin_id]: [string] = await trx(tableNameAdmin)
          .returning("admin_id")
          .insert(adminToSave)
        upsertAdminId = newAdmin_id
      }
    }

    await trx.commit()
  } catch (e) {
    await trx.rollback()
    const message = e && e.sqlMessage ? e.sqlMessage : e
    throw message
  }

  return { admin_id: upsertAdminId } as IAdminJSON
}

export const comparePasswordAndHash = (password, hash) => {
  return new Promise(resolve => {
    if (bcrypt.compareSync(password, hash)) {
      resolve(true)
    } else {
      resolve(false)
    }
  })
}

type TAdminsById = Record<string, IAdminJSON>
class Admin {
  private qb: QueryBuilder
  constructor() {
    this.qb = db(tableNameAdmin)
  }
  knex() {
    return this.qb
  }
  async run(): Promise<IAdminJSON[]> {
    return await this.qb
  }
  select(withPassword?: boolean) {
    this.qb = this.qb
      .select(
        [
          "admin_id",
          "name",
          "email",
          withPassword ? "password" : null,
        ].filter(f => f)
      )
    return this
  }
  list() {
    this.qb = this.select().knex()
    return this
  }
  getById(id: number | string) {
    this.qb = this.select()
      .knex()
      .where({ admin_id: id })
    return this
  }
  getByName(name: string, withPassword?: boolean) {
    this.qb = this.select(withPassword)
      .knex()
      .where({ name: name })
    return this
  }
  getByEmail(email: string, withPassword?: boolean) {
    this.qb = this.select(withPassword)
      .knex()
      .where({ email: email })
    return this
  }
  update(id: number | string, adminJSON: IAdminJSON) {
    this.qb = this.select()
      .knex()
      .where({ admin_id: id })
      .first()
      .update(adminJSON)
    return this
  }
}

export const getAdminById = async (id: string | number): Promise<IAdminJSON> => {
  const [admin] = await new Admin().getById(id).run()
  return admin
}

export const getAdminByEmail = async (email: string, withPassword?: boolean): Promise<IAdminJSON> => {

  console.log(email)

  const [admin] = await new Admin().getByEmail(email, withPassword).run()
  console.log(admin)
  return admin
}

export const getAdminsList = async (): Promise<IAdminJSON[]> => {
  return await new Admin().list().run()
}
