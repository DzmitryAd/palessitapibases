import * as dotenv from "dotenv"
dotenv.config()


import * as Koa from "koa"
import * as bodyParser from "koa-bodyparser"

import { errorsMiddleware } from "./middleware/errors"
import { adminMiddleware } from "./middleware/adminMiddleware"
import { env } from "./util/env"
import { router } from "./routes/routes"

const app = new Koa()

const bootstrap = async () => {
  app
    .use(errorsMiddleware)
    .use(bodyParser())
    .use(adminMiddleware)
    .use(router.routes())
    .use(router.allowedMethods())

  app.listen(env.PORT)

  console.log(`PORT:${env.PORT} NODE_ENV:${env.NODE_ENV}`)
}

bootstrap()


