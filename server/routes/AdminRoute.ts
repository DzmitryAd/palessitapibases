import * as Router from "koa-router"

import {
  getAdminsList,
  getAdminById,
  upsertAdmin,
  IAdminJSON
} from "../models/AdminModel"
import { IAppContext } from "../models/App"

export const adminRoute = new Router()

adminRoute
  .get("/", async (ctx: IAppContext, next) => {
    // access check is in route middleware
    const admins = await getAdminsList()
    ctx.body = admins
  })
  .get("/:id", async (ctx: IAppContext, next) => {
    // access check is in route middleware
    const { id } = ctx.params
    const admin = await getAdminById(id)
    ctx.body = admin
  })
  .post("/:id", async (ctx: IAppContext, next) => {
    // access check is in route middleware
    const { id } = ctx.params
    const adminJSON: IAdminJSON = ctx.request.body
    await upsertAdmin(adminJSON, id)
    ctx.status = 200
  })
