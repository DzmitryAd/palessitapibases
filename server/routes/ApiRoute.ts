import * as Router from "koa-router"

import { adminRoute } from "./AdminRoute"
import { accountRoute } from "./AccountRoute"
import { IAppContext } from "../models/App"

export const apiRoute = new Router()

apiRoute
  .use("/account", accountRoute.routes())
  .use(accountRoute.allowedMethods())

apiRoute.use(async (ctx: IAppContext, next) => {
  const { admin } = ctx.state
  if (admin) {
    await next()
  } else {
    ctx.throw(403)
  }
})

apiRoute
  .use("/admins", adminRoute.routes())
  .use(adminRoute.allowedMethods())
