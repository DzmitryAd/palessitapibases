import * as Router from "koa-router"

import { IAppContext, ContextAdmin } from "../models/App"
import {
  getAdminByEmail,
  comparePasswordAndHash,
  getAdminById,
  IAdminJSON
} from "../models/AdminModel"
import { createJwt } from "../util/jwt"

const loginAnswer = async (ctx: IAppContext, adminJSON: IAdminJSON) => {
  const token = createJwt({ admin_id: adminJSON.admin_id })
  ctx.append("Token", token)
  ctx.status = 200
}

const login = async (ctx: IAppContext) => {
  const { email, password } = ctx.request.body
  const admin = await getAdminByEmail(email, true)

  const noMatchMessage = "No matched email and password found"
  if (!admin) {
    ctx.throw(403, noMatchMessage)
  }

  const isVerified = await comparePasswordAndHash(password, admin.password)

  if (!isVerified) {
    ctx.throw(403, noMatchMessage)
  }

  loginAnswer(ctx, admin)
}

const loginWithToken = async (ctx: IAppContext) => {
  if (!ctx.state.admin) {
    ctx.throw(403, "Invalid authentication token")
  }
  ctx.body = {
    user: ctx.state.admin
  }
}

export const accountRoute = new Router()
accountRoute
  .post("/login", login)
  .get("/loginWithToken", loginWithToken)
