import * as Router from "koa-router"
import { apiRoute } from "./ApiRoute"

export const router = new Router()

router
  .use("", apiRoute.routes())
  .use(apiRoute.allowedMethods())
